package ar.fiuba.tdd.tp1.model;

/**
 * Created by marcelo on 04/10/15.
 * Error handling.
 */
public class BadFormulaException extends RuntimeException {
    public BadFormulaException(String cellExceptionMsg) {
        System.err.println(cellExceptionMsg);
    }
}