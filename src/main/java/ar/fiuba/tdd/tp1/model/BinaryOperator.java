package ar.fiuba.tdd.tp1.model;

import java.util.HashMap;

/**
 * Created by marcelo on 03/10/15.
 * Binary operators implementation
 */

public class BinaryOperator implements CellExpression {

    enum MathOperator {


        ADD("+") {
            public double execute(double operand1, double operand2) {
                return operand1 + operand2;
            }
        },
        SUB("-") {
            public double execute(double operand1, double operand2) {
                return operand1 - operand2;
            }

        };

        private final String op;

        MathOperator(String operator) {
            this.op = operator;
        }

        public abstract double execute(double operand1, double operand2);
    }

    private CellExpression left = null;
    private CellExpression right = null;
    private String operator = null;

    public BinaryOperator(CellExpression lhs, CellExpression rhs, String operator) {
        this.right = rhs;
        this.left = lhs;
        this.operator = operator;
    }


    public double interpret(HashMap<String,Double> cellRefs) {
        for (MathOperator m: MathOperator.values()) {
            if (m.op.equals(operator)) {
                return m.execute(left.interpret(cellRefs), right.interpret(cellRefs));
            }
        }
        throw( new BadFormulaException("Interpreter got an exception. Invalid operator."));
    }
}
