package ar.fiuba.tdd.tp1.model;


/**
 * Created by Nico on 21/9/2015.
 *  Basic Book Structure.
 */

import ar.fiuba.tdd.tp1.model.functions.FunctionsManager;
import ar.fiuba.tdd.tp1.model.functions.functionsimpl.ChangedCommand;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Book implements Workbook {

    private List<Sheet> sheets;
    private int indexSheet = 1;
    private Sheet activeSheet;
    private String name;
    private Formula formulas;

    public Book() {
        this.sheets = new ArrayList<>();
        this.addSheet("default");
        this.activeSheet = this.sheets.get(0);
        this.formulas = new Formula();
    }

    public Book(String bookName) {
        this.sheets = new ArrayList<>();
        this.addSheet("default");
        this.activeSheet = this.sheets.get(0);
        this.formulas = new Formula();
        this.name = bookName;
    }

    private void doAddSheet(Sheet sheet) {
        this.sheets.add(sheet);
        ChangedCommand sheetAdded = new ChangedCommand();
        sheetAdded.setUndoValues(null, sheet);
        FunctionsManager.getInstance().addOperation(sheetAdded);
    }

    @Override
    public List<Sheet> getSheets() {
        return sheets;
    }

    @Override
    public void setSheets(List<Sheet> sheets) {
        this.sheets = sheets;
    }

    @Override
    public void removeSheet(String name) {
        Sheet sheet = this.getSheet(name);
        if (sheet != null) {
            ChangedCommand deleted = new ChangedCommand();
            deleted.setUndoValues(sheet, null);
            FunctionsManager.getInstance().addOperation(deleted);
            this.sheets.remove(sheet);
        }
    }

    @Override
    public void addSheet(String name) {
        Sheet sheet = new Sheet(name);
        doAddSheet(sheet);
    }

    @Override
    public void addSheet() {
        Sheet sheet = new Sheet("Sheet" + indexSheet++);
        doAddSheet(sheet);
    }

    @Override
    public Sheet getSheet(String name) {
        List<Sheet> sheetList = sheets.stream()
                .filter(s -> s.getName().equals(name))
                .collect(Collectors.toList());
        if (sheetList != null) {
            return sheetList.get(0);
        } else {
            return null;
        }
    }

    @Override
    public Sheet getActiveSheet() {
        return activeSheet;
    }

    @Override
    public Object getCellValueByName(String cellRef) {
        if (!ExpressionTerms.isValidTerm(cellRef, ExpressionTerms.CELLREF)) {
            throw new IllegalArgumentException("Invalid cell reference format");
        }
        Object val;
        String[] elements = cellRef.split(":");
        val = getCellValue(elements[0], elements[1]);
        return val;
    }

    @Override
    public Object getCellValue(String sheetName, String cellId) {
        Object val;
        try {
            Sheet sheet = this.getSheet(sheetName);
            List<String> rowAndCol = ExpressionTerms.getRowAndColId(cellId);
            val = sheet.getCellValue(rowAndCol.get(0), rowAndCol.get(1));
            if (val instanceof String) {
                val = String.valueOf(val);
            } else {
                return val;
            }
        } catch (Exception e) {
            return null;
        }
        //if (val == null) {
        //    return null;
        //}
        if (!ExpressionTerms.isValidTerm((String) val, ExpressionTerms.CELLFORMULA)) {
            return val;
        } else {
            double parser = new CellParser().evaluate(((String) val).substring(2), this);
            return parser;
        }
    }

    @Override
    public void setCellValue(String workSheetName, String cellId, Object value) {

        Sheet sheet = this.getSheet(workSheetName);
        List<String> rowAndCol = ExpressionTerms.getRowAndColId(cellId);
        String parsedValue;

        if (!value.getClass().getSimpleName().equals("String")) { // No formulas allowed on cell types != String
            sheet.setCell(Integer.parseInt(rowAndCol.get(0)), rowAndCol.get(1), value);
        } else {
            if (ExpressionTerms.isValidTerm((String) value, ExpressionTerms.CELLFORMULA)
                    || ExpressionTerms.isValidTerm((String) value, ExpressionTerms.ROWCOLREF)) {
                parsedValue = ExpressionTerms.convertToFullRef(((String) value).substring(2), workSheetName);
                if (!formulas.add(workSheetName + ":" + rowAndCol.get(1) + rowAndCol.get(0), parsedValue.substring(2))) {
                    throw new RuntimeException("bad formula");
                } else {
                    sheet.setCell(Integer.parseInt(rowAndCol.get(0)), rowAndCol.get(1), parsedValue);
                }
            } else {
                sheet.setCell(Integer.parseInt(rowAndCol.get(0)), rowAndCol.get(1), value);
            }
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean existSheet(String name) {
        return sheets.stream().anyMatch(p -> (p.getName()).equals(name));
    }

    @Override
    public List<String> getSheetNames() {
        List<String> sheetNames = new ArrayList<>();
        for (Sheet s : sheets) {
            sheetNames.add(s.getName());
        }
        return sheetNames;
    }
}