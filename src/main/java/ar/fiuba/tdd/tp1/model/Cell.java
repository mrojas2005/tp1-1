package ar.fiuba.tdd.tp1.model;

import static ar.fiuba.tdd.tp1.model.CellTypes.DataType;
import static ar.fiuba.tdd.tp1.model.CellTypes.getType;

/**
 * Created by Nico on 21/9/2015.
 * Basic Cell Structure and its methods.
 */
public class Cell<T> {

    private T value;
    private String positionCol;
    private int positionRow;
    private CellOutFormatter formatter;

    public Cell(T value, int positionRow, String positionCol) {
        this.value = value;
        this.positionCol = positionCol;
        this.positionRow = positionRow;
        this.formatter = new CellOutFormatter();
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public String getPositionCol() {
        return positionCol;
    }

    public Integer getPositionRow() {
        return positionRow;
    }

    public void setPositionRow(Integer positionRow) {
        this.positionRow = positionRow;
    }

    public Cell<T> copy() {
        return new Cell<>(this.value, this.positionRow, this.positionCol);
    }

    public String getParameter(String key) {
        return formatter.getAttrValue(key);
    }

    public void setParameter(String key, String value) {

        formatter.setAttrValue(key, value);
    }

    public String getFormattedData() {
        try {
            DataType thisClass = getType(this.value.getClass());
            String outdata;
            assert thisClass != null;
            outdata = thisClass.getFormattedData(this);
            return outdata;
        } catch (NullPointerException e) {
            throw new BadFormulaException("Error en impresión de celda");
        }
    }

    public void setDecimals(int count) {
        try {
            DataType thisClass = getType(this.value.getClass());
            assert thisClass != null;
            thisClass.setDecimals(this, count);
        } catch (Exception e) {
            throw new IllegalArgumentException("Error en seteo de decimales");
        }
    }

    public void setOutputFormatter(String formatter) {
        try {
            DataType thisClass = getType(this.value.getClass());
            assert thisClass != null;
            thisClass.setOutputFormatter(this,formatter);
        } catch (Exception e) {
            throw new IllegalArgumentException("Error en seteo de formato");
        }
    }

    public void setCurrencyCode(String currencyCode) {
        try {
            ((Money) this.value).setCurrencyCode(currencyCode);
        } catch (Exception e) {
            System.err.println("Error en seteo de moneda");
        }
    }
}
