package ar.fiuba.tdd.tp1.model;

import java.lang.*;
import java.util.HashMap;


/**
 * Created by marcelo on 10/2/15.
 * Interfaz de Celda
 */

interface CellExpression {
    double interpret(HashMap<String, Double> context);

}








