package ar.fiuba.tdd.tp1.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by marcelo on 10/10/15.
 */
public class CellOutFormatter {
    Map<String, String> attrList;

    public CellOutFormatter() {
        attrList = new HashMap<>();
    }

    public Map<String, String> getAttrList() {
        return attrList;
    }

    public void setAttrValue(String key, String value) {
        attrList.put(key, value);
    }

    public String getAttrValue(String key) {
        return attrList.get(key);
    }

    public void removeAttrValue(String key, String value) {
        attrList.remove(key, value);
    }

    public void removeAllValues(String key) {
        attrList.remove(key);
    }
}
