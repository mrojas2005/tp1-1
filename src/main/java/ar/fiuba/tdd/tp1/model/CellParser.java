package ar.fiuba.tdd.tp1.model;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by marcelo on 26/09/15.
 *
 * <p>
 * GRAMMAR:
 *
 * <p>
 * expression ::= cell | number | plus | minus
 * cell ::= sheet:col:row
 * sheet ::= [a-zA-Z0-9]
 * row ::= digit | digit digit...
 * col ::= letter | letter letter...
 * digit ::= 0 | 1 | ..... | 9
 * letter ::= 'A' | ..... | 'Z' | letter
 * plus ::= '+'
 * minus ::= '-'
 *
 * <p>
 */

public class CellParser {

    private static final int NOERROR = 0;
    private static final int SYNTAX_ERROR = 1;
    private static final int INVALID_REFERENCE = 2;

    private int statusCode = 0;

    public CellParser() {
        statusCode = NOERROR;
    }

    private static void checkCellReference(String item, Stack<CellExpression> stack) {

        if (ExpressionTerms.isValidTerm(item, ExpressionTerms.CELLREF)) { // Numbers or Variables are terminal expressions
            CellReferenceExpression expr = new CellReferenceExpression(item);
            stack.push(expr);
        }
    }

    private static void checkNumber(String item, Stack<CellExpression> stack) {

        if (ExpressionTerms.isValidTerm(item, ExpressionTerms.NUMBER)) {
            Constant expr = new Constant(Double.parseDouble(item));
            stack.push(expr);
        }
    }

    private static void checkOperator(String item, Stack<CellExpression> stack) {
        CellExpression rhs;
        CellExpression lhs;
        CellExpression expr;

        if (ExpressionTerms.isValidTerm(item, ExpressionTerms.OPERATOR)) { // Operators take 2 arguments
            rhs = stack.pop();
            lhs = stack.pop();
            expr = new BinaryOperator(lhs, rhs, item);
            stack.push(expr);
        }

    }

    private static CellExpression createExpressionTree(List<String> items) {

        Stack<CellExpression> stack = new Stack<>();

        for (String item : items) {
            checkOperator(item, stack);
            checkCellReference(item, stack);
            checkNumber(item, stack);
        }

        return stack.pop();

    }

    private int getError() {
        return statusCode;
    }

    /*
    Creates a syntax tree for the given cell expression
     */

    private void setError(int err) {
        statusCode = err;
    }

    private List<String> getCellRefs(List<String> items) {

        return items.stream()
                .filter(p -> ExpressionTerms.isValidTerm(p, ExpressionTerms.CELLREF))
                .collect(Collectors.toList());

    }

    private HashMap<String, Double> getCellRefsList(List<String> items, Book book) {

        HashMap<String, Double> cellRefsList = new HashMap<>();
        List<String> cellRefs = getCellRefs(items);

        for (String cellRef: cellRefs) {
            Object val = book.getCellValueByName(cellRef);
            if (val == null) {
                setError(INVALID_REFERENCE);
                return null;
            }
            if (!ExpressionTerms.isValidTerm(val.toString(), ExpressionTerms.NUMBER)) {
                setError(SYNTAX_ERROR);
                return null;
            }
            cellRefsList.put(cellRef, Double.parseDouble(val.toString()));
        }
        return cellRefsList;
    }

    public double evaluate(String expression, Book book) {

        List<String> items = new ArrayList<>(Arrays.asList(expression.split(" ")));
        List<String> postfix = PostfixParser.convertToPostfix(items);

        if (postfix == null) {
            this.setError(SYNTAX_ERROR);
            throw new IllegalArgumentException("Syntax error parsing " + expression);
        }

        HashMap<String, Double> cellRefs = getCellRefsList(postfix, book);
        CellExpression expr = createExpressionTree(postfix);

        return expr.interpret(cellRefs);


    }
}
