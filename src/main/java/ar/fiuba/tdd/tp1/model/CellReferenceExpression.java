package ar.fiuba.tdd.tp1.model;

/**
 * Created by marcelo on 03/10/15.
 * Implementacion que maneja referencias a celdas
 */

import java.util.HashMap;


class CellReferenceExpression implements CellExpression {

    private String value = null;

    public CellReferenceExpression(String cellRef) {
        this.value = cellRef;
    }

    public double interpret(HashMap<String, Double> cellRefs) {
        double result;
        try {
            result = cellRefs.get(value);
            return result;
        } catch (Exception e) {
            throw new RuntimeException("Invalid cell value " + value);
        }

    }
}