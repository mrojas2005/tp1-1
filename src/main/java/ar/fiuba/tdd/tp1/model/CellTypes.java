package ar.fiuba.tdd.tp1.model;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


/**
 * Created by marcelo on 10/10/15.
 * Manejo de tipos de datos en celdas.
 */
public class CellTypes {

    private static final String DEFPATTERN  = "###,###,###";
    private static final int DECIMAL = 0;

    public static DecimalFormatSymbols setLocaleSymbols() {
        Locale locale = new Locale("es", "AR");
        DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
        symbols.setDecimalSeparator('.');
        symbols.setGroupingSeparator(',');
        return symbols;
    }

    public static void checkValidType(Object cellvalue) {
        for (DataType data : DataType.values()) {
            if (data.className.isAssignableFrom(cellvalue.getClass())) {
                return;
            }
        }
        throw new BadFormulaException("Not a valid cell data type:");
    }

    public static DataType getType(Class cellClass) {
        for (DataType data : DataType.values()) {
            if (!data.className.isAssignableFrom(cellClass)) {
                continue;
            }
            return data;
        }
        return null;
    }

    private static String setDecimalFormat(int count) {
        String pattern = DEFPATTERN;
        if (count > 0) {
            pattern = pattern.concat(".");
            for (int i = 0; i < count; i++) {
                pattern = pattern.concat("#");
            }
        }
        return pattern;
    }

    public enum DataType {


        Number(Number.class) {



            public void setDecimals(Cell cell, int count) {
                cell.setParameter("Number.Decimal",Integer.toString(count));
                setOutputFormatter(cell, setDecimalFormat(count));
            }

            public void setOutputFormatter(Cell cell, String pattern) {
                if (pattern != null) {
                    cell.setParameter("Number.Format", pattern);
                }
                if (cell.getParameter("Number.Decimal") == null) {
                    setDecimals(cell,DECIMAL);
                }
            }

            public String getFormattedData(Cell cell) {
                String format = cell.getParameter("Number.Format");
                String outdata;
                if (format != null) {
                    outdata = new DecimalFormat(format, setLocaleSymbols()).format(cell.getValue());
                } else {
                    outdata = cell.getValue().toString();
                }
                return outdata;
            }
        },
        Money(Money.class) {

            public void setOutputFormatter(Cell cell, String pattern) {
                cell.setParameter("Money.Format", pattern);
            }

            public String getFormattedData(Cell cell) {
                if (cell.getParameter("Money.Format") == null) {
                    cell.setParameter("Money.Format",DEFPATTERN);
                }
                NumberFormat numberFormat = new DecimalFormat(cell.getParameter("Money.Format"), setLocaleSymbols());
                Money money = (Money) cell.getValue();
                return money.getCurrencyCode() + " " + numberFormat.format(money.getAmount());
            }

            public void setDecimals(Cell cell, int count) {
                cell.setParameter("Money.Decimal", Integer.toString(count));
                setOutputFormatter(cell, setDecimalFormat(count));
            }
        },
        String(String.class) {
            public void setOutputFormatter(Cell cell, String pattern) {
            }

            public String getFormattedData(Cell cell) {
                return (String) cell.getValue();
            }

            public void setDecimals(Cell cell, int count) {
                throw new IllegalArgumentException("Decimals not applicable to String data type");
            }
        },
        Date(Date.class) {
            public void setOutputFormatter(Cell cell, String pattern) {
                cell.setParameter("Date.Format", pattern);
            }

            public String getFormattedData(Cell cell) {
                setLocaleSymbols();
                String format = cell.getParameter("Date.Format");
                String outdata;
                if (format != null) {
                    SimpleDateFormat data = new SimpleDateFormat(format);
                    outdata = data.format(cell.getValue());

                } else {
                    outdata = cell.getValue().toString();
                }
                return outdata;
            }

            public void setDecimals(Cell cell, int count) {
                throw new IllegalArgumentException("Decimals are not applicable to Date data type");
            }
        };
        private final Class className;

        DataType(Class classtype) {
            this.className = classtype;
        }

        public abstract void setOutputFormatter(Cell cell, String pattern);

        public abstract String getFormattedData(Cell cell);

        public abstract void setDecimals(Cell cell, int count);

    }
}
