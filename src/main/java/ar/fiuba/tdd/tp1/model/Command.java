package ar.fiuba.tdd.tp1.model;
/**
 * Created by Carlos on 11/10/2015.
 */
public interface Command {

    void execute();

    void undo();

    void redo();
}