package ar.fiuba.tdd.tp1.model;
/**
 * Created by Carlos on 11/10/2015.
 */
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;
import java.lang.System;

public class CommandDeleteSheet implements Command {

    private Workbook workbook;
    private String sheetName;

    public CommandDeleteSheet(Workbook workbook, String workSheetName) {
        this.workbook = workbook;
        this.sheetName = workSheetName;
    }

    @Override
    public void execute() {
        this.workbook.removeSheet(this.sheetName);
    }

    @Override
    public void undo() {
        this.workbook.addSheet(this.sheetName);
    }

    @Override
    public void redo() {
        // TODO no need to complete
    }
}