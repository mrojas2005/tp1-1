package ar.fiuba.tdd.tp1.model;
/**
 * Created by Carlos on 11/10/2015.
 */
import java.util.Stack;

public class CommandManager {

    private Stack<Command> undos;
    private Stack<Command> redos;

    public CommandManager() {
        undos = new Stack<Command>();
        redos = new Stack<Command>();
    }

    public void executeCommand(Command command) {
        command.execute();
        undos.push(command);
        redos.clear();
    }

    public void undo() {
        if (!undos.empty()) {
            Command command = undos.pop();
            command.undo();
            redos.push(command);
        }
    }

    public void redo() {
        if (!redos.empty()) {
            Command command = redos.pop();
            command.execute();
            undos.push(command);
        }
    }

    public boolean isUndoAvailable() {
        return !undos.empty();
    }

    public boolean isRedoAvailable() {
        return !redos.empty();
    }
}