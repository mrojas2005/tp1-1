package ar.fiuba.tdd.tp1.model;
/**
 * Created by Carlos on 11/10/2015.
 */
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;
import java.lang.System;

public class CommandSet implements Command {

    private Workbook workbook;
    private String sheetName;
    private String cellId;
    private Object value;
    private Object valueBK;

    public CommandSet(Workbook workbook, String workSheetName, String cellId, Object value) {
        this.workbook = workbook;
        this.sheetName = workSheetName;
        this.cellId = cellId;
        this.value = value;
        this.valueBK = value;
    }

    @Override
    public void execute() {
        this.valueBK = this.workbook.getCellValue(this.sheetName, this.cellId);
        this.workbook.setCellValue(this.sheetName, this.cellId, this.value);
    }

    @Override
    public void undo() {
        this.workbook.setCellValue(this.sheetName, this.cellId, this.valueBK);
    }

    @Override
    public void redo() {
        // TODO no need to complete
    }

    public Object getValueBK() {
        return this.valueBK;
    }
}