package ar.fiuba.tdd.tp1.model;

import java.util.HashMap;

/**
 * Created by marcelo on 03/10/15.
 * Implementación de una constante
 */

class Constant implements CellExpression {

    private double value;

    public Constant(double val) {
        this.value = val;
    }

    public double interpret(HashMap<String,Double> cellRefs) {
        return this.value;
    }
}