package ar.fiuba.tdd.tp1.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by marcelo on 10/2/15.
 * utilities
 */
public class ExpressionTerms {

    public static final String CELLREF = "(.*:)+[A-Z]+[0-9]+";
    public static final String ROWCOLREF = "^[A-Z]+[0-9]+";
    public static final String NUMBER = "-?\\d+(\\.\\d+)?";
    public static final String OPERATOR = "\\+|-";
    public static final String CELLFORMULA = "^= .*";
    public static final String CELLREFSEP = ":";

    public static boolean isValidOperand(String token) {

        return isValidTerm(token,CELLREF) || isValidTerm(token,NUMBER);
    }

    public static boolean isValidTerm(String token, String pattern) {
        final Pattern regex = Pattern.compile(pattern);

        return regex.matcher(token).matches();
    }

    public static List<String> getRowAndColId(String colRow) { // returns row, col
        List<String> result = new ArrayList<>();
        result.add(0, colRow.replaceAll("[A-Z]",""));
        result.add(1,colRow.replaceAll("[0-9]",""));
        return result;
    }

    public static String convertToFullRef(String expression, String workSheetName) {
        List<String> tokens = new ArrayList<>(Arrays.asList(expression.split(" ")));
        String result = "=";
        for (String token : tokens) {
            if (!ExpressionTerms.isValidTerm(token, ExpressionTerms.CELLREF)
                    && ExpressionTerms.isValidTerm(token, ExpressionTerms.ROWCOLREF)) {
                result = result + " " + workSheetName + ":" + token;
            } else {
                result = result.concat(" " + token);
            }
        }
        return result.replaceAll("!", "");
    }

}
