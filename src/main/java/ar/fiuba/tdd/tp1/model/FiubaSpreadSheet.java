package ar.fiuba.tdd.tp1.model;
/**
 * Created by Carlos on 09/10/2015.
 * Interfaz a ser utilizada por el cliente
 */


import java.util.List;

public interface FiubaSpreadSheet {

    void createBook(String bookName);

    void deleteBook(String bookName);

    boolean existBook(String bookName);

    Workbook getBook(String bookName);

    void createSheet(String bookName, String sheetName);

    void deleteSheet(String bookName, String sheetName);

    boolean existSheet(String bookName, String sheetName);

    void setCellValue(String workBookName, String workSheetName, String cellId, Object value);

    // devuelve el valor calculado de la celda
    Object getCellValueByName(String bookName, String cellRef);

    // deshace la ultima operacion
    void undo();

    // rehace la ultima operacion
    void redo();

    List<String> getWorkbookNames();

    List<String> workSheetNamesFor(String workbookName);

    String getCellValueAsString(String workBookName, String workSheetName, String cellId);
}