package ar.fiuba.tdd.tp1.model;

import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleDirectedGraph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by marcelo on 10/9/15.
 */
public class Formula {
    private SimpleDirectedGraph<String,DefaultEdge> formulasGraph;

    public Formula() {
        formulasGraph = new SimpleDirectedGraph<>(DefaultEdge.class);
    }

    public boolean remove(String cellId) {
        return true;
    }

    public boolean add(String cellName, String expression) {

        List<String> tokens = new ArrayList<>(Arrays.asList(expression.split(" ")));
        List<String> endVertex = tokens.stream()
                .filter(token -> ExpressionTerms.isValidTerm(token,ExpressionTerms.CELLREF))
                .collect(Collectors.toList());

        formulasGraph.addVertex(cellName); // origin vertex
        for (String vertex: endVertex) {
            if (!formulasGraph.addVertex(vertex)) { // ending vertex
                return false;
            } else {
                formulasGraph.addEdge(cellName, vertex); // connecting edge
            }
        }
        return true;
    }
}

