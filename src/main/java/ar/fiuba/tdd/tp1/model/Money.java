package ar.fiuba.tdd.tp1.model;

import java.io.Serializable;
import java.util.Currency;
import java.util.Locale;

/**
 * Created by marcelo on 11/10/15.
 * Implementación mínima de una clase "Money" para manejar cantidad y moneda.
 * Manejo de monedas según la clase nativa Currency
 */
public final class Money implements Serializable {

    private static final long serialVersionUID = 1L;

    private Currency currency;

    private double amount;


    public Money(String currencyCode, double amount) {
        this.currency = Currency.getInstance(currencyCode);
        this.amount = amount;
    }

    public Money() {
        Locale currentLocale = new Locale("es", "AR");
        this.currency = Currency.getInstance(currentLocale);
        this.amount = 0;
    }

    public Money(double amount) {
        Locale currentLocale = new Locale("es", "AR");
        this.currency = Currency.getInstance(currentLocale);
        this.amount = amount;
    }

    public double getAmount() {
        return this.amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Currency getCurrency() {
        return this.currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getCurrencyCode() {
        return this.currency.getCurrencyCode();
    }

    public void setCurrencyCode(String currencyCode) {
        this.currency = Currency.getInstance(currencyCode);
    }

}
