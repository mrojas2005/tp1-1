package ar.fiuba.tdd.tp1.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created by marcelo on 04/10/15.
 * Method for convert infix to postfix notation.
 */
public class PostfixParser {

    public static List<String> convertToPostfix(List<String> items) {

        List<String> result = new ArrayList<>();
        Stack<String> operators = new Stack<>();
        int itemsIndex = 0;

        try {
            addOperand(items.get(itemsIndex), result); // 1 operand only (case "=<cellref>")
            itemsIndex++;

            while (itemsIndex < items.size()) { // zero or more <operator> <operand>
                addOperator(items.get(itemsIndex), result, operators);
                itemsIndex++;
                addOperand(items.get(itemsIndex), result);
                itemsIndex++;
            }
        } catch (BadFormulaException e) {
            System.err.println("Invalid formula.");
            e.printStackTrace();
            return null;
        }
        while (!operators.isEmpty()) {
            result.add(operators.pop());
        }

        return result;
    }

    private static String addOperand(String item, List<String> result) {

        if (ExpressionTerms.isValidOperand(item)) {
            result.add(item);
            return item;
        } else {
            throw (new RuntimeException("Invalid operand " + item));
        }

    }

    private static String addOperator(String item, List<String> result, Stack<String> operators) {
        if (ExpressionTerms.isValidTerm(item, ExpressionTerms.OPERATOR)) {
            if (!operators.isEmpty()) {
                result.add(operators.pop());
            }
            operators.push(item);
            return item;
        } else {
            throw (new BadFormulaException("Invalid operator " + item));
        }
    }
}