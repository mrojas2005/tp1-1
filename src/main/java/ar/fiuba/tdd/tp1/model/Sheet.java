package ar.fiuba.tdd.tp1.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Nico on 21/9/2015.
 * Basic Sheet structure.
 */
public class Sheet {

    private String name;
    private Map<Integer,List<Cell>> rows;


    public Sheet() {
        rows = new HashMap<>();
        this.initRows(1);
    }

    public Sheet(String name) {
        this.name = name;
        rows = new HashMap<>();
        this.initRows(1);
    }

    private void initRows(int rowNumber) {
        this.rows.put(rowNumber, new ArrayList<>()); // Default creation with 1 row, the first one.
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private boolean rowExists(int row) {
        return rows.containsKey(row);
    }


    public Map<Integer, List<Cell>> getRows() {
        return rows;
    }

    public void setRows(Map<Integer, List<Cell>> cells) {
        this.rows = cells;
    }


    public void setCell(Integer positionRow, String positionCol, Object value) {

        CellTypes.checkValidType(value);

        Cell cell = new Cell<>(value, positionRow, positionCol);

        if (!rowExists(positionRow)) {
            this.initRows(positionRow);
        }

        if (this.rows.get(positionRow) != null) {
            this.rows.get(positionRow).removeIf(p -> p.getPositionCol().equals(positionCol));
        }
        this.rows.get(positionRow).add(cell);
        this.rows.get(positionRow).sort(
                (Cell cell1, Cell cell2) -> cell1.getPositionCol().compareToIgnoreCase(cell2.getPositionCol()));
    }

    public Object getCellValue(String rowId, String columnId) {

        Cell cell = this.getCell(rowId, columnId);
        if (cell != null) {
            return cell.getValue();
        }
        return null;
    }

    public Cell getCell(String cellId) {
        List<String> rowAndCol = ExpressionTerms.getRowAndColId(cellId);
        return this.getCell(rowAndCol.get(0), rowAndCol.get(1));
    }

    public Cell getCell(String rowId, String columnId) {

        int row = Integer.parseInt(rowId);
        List<Cell> rowCells = this.rows.getOrDefault(row,null);
        List<Cell> cell = rowCells.stream()
                .filter(s -> s.getPositionCol().equals(columnId))
                .collect(Collectors.toList());

        if (cell.size() > 0) {
            return cell.get(0);
        }
        return null;
    }



}
