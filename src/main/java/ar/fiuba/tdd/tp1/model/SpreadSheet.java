package ar.fiuba.tdd.tp1.model;
/**
 * Created by Carlos on 09/10/2015.
 * implementación de interfaz de fiubaspreadsheet.
 */

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SpreadSheet implements FiubaSpreadSheet {

    private List<Workbook> workbooks;
    private CommandManager commandMgr;

    public SpreadSheet() {
        workbooks = new ArrayList<>();
        commandMgr = new CommandManager();
    }

    @Override
    public void createBook(String bookName) {
        workbooks.add(new Book(bookName));
    }

    @Override
    public void deleteBook(String bookName) {
        workbooks.removeIf(p -> p.getName().equals(bookName));
    }

    @Override
    public boolean existBook(String bookName) {
        return workbooks.stream().anyMatch(p -> (p.getName()).equals(bookName));
    }

    @Override
    public Workbook getBook(String workbookName) {
        try {
            return workbooks.stream().filter(p -> p.getName().equals(workbookName)).findFirst().get();
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }


    @Override
    public void createSheet(String workbookName, String sheetName) {
        Workbook book = getBook(workbookName);
        Command command = new CommandCreateSheet(book, sheetName);
        this.commandMgr.executeCommand(command);
    }

    @Override
    public void deleteSheet(String workbookName, String sheetName) {
        Workbook book = getBook(workbookName);
        Command command = new CommandDeleteSheet(book, sheetName);
        this.commandMgr.executeCommand(command);
    }

    @Override
    public boolean existSheet(String workbookName, String sheetName) {
        //TODO en caso de null retornar exception
        Workbook book = getBook(workbookName);
        return book != null && book.existSheet(sheetName);
    }

    public void setCellValue(String workbookName, String workSheetName, String cellId, Object value) {
        Workbook book = getBook(workbookName);
        Command command = new CommandSet(book, workSheetName, cellId, value);
        this.commandMgr.executeCommand(command);
    }

    @Override
    public Object getCellValueByName(String workbookName, String cellRef) {
        Workbook book = getBook(workbookName);
        return book.getCellValueByName(cellRef);
    }

    @Override
    public void undo() {
        commandMgr.undo();
    }

    @Override
    public void redo() {
        commandMgr.redo();
    }

    @Override
    public List<String> getWorkbookNames() {
        List<String> bookNames = new ArrayList<>();
        for (Workbook b : workbooks) {
            bookNames.add(b.getName());
        }
        return bookNames;
    }

    @Override
    public List<String> workSheetNamesFor(String workbookName) {
        Workbook book = getBook(workbookName);
        List<String> result;
        return book.getSheetNames();
    }

    @Override
    public String getCellValueAsString(String workbookName, String workSheetName, String cellId) {
        Workbook book = getBook(workbookName);
        return book.getCellValueByName(workSheetName + ":" + cellId).toString();
    }

}