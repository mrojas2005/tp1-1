package ar.fiuba.tdd.tp1.model;
/**
 * Created by Carlos on 09/10/2015.
 * Manejo de books.
 */
import java.util.List;

public interface Workbook {

    List<Sheet> getSheets();

    void setSheets(List<Sheet> sheets);

    void removeSheet(String name);

    void addSheet(String name);

    void addSheet();

    Sheet getSheet(String name);

    Sheet getActiveSheet();

    Object getCellValueByName(String cellRef);

    Object getCellValue(String sheetName, String cellId);

    void setCellValue(String workSheetName, String cellId, Object value);

    String getName();

    void setName(String name);

    boolean existSheet(String name);

    List<String> getSheetNames();
}
