package ar.fiuba.tdd.tp1.model.filehandlers;

import ar.fiuba.tdd.tp1.model.Book;

/**
 * Created by Nico on 11/10/2015. Interfaz para invocar multiples inputs
 */
public interface FileHandler {

    Book createBookFromFile(String filename);
}
