package ar.fiuba.tdd.tp1.model.filehandlers;

import ar.fiuba.tdd.tp1.model.Book;
import ar.fiuba.tdd.tp1.model.Cell;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nico on 11/10/2015. Maneja input de archivos de tipo CSV
 */

public class FileHandlerCSV implements FileHandler {

    private  String valuesSeparator =  ",";

    @Override
    public Book createBookFromFile(String filename) {
        Book book = new Book(filename.substring(0,filename.lastIndexOf(".")));
        try {
            //TODO VERIFICAR SI CONVIENE MANTENER ACA LOGICA APERTURA DE ARCHIVO
            Reader reader = new InputStreamReader(new FileInputStream(filename), "UTF-8");
            BufferedReader br = new BufferedReader(reader);
            String csvRows;
            int rows = 0;
            while ((csvRows = br.readLine()) != null) {
                String[] csvCells = csvRows.split(this.getValuesSeparator());
                List<Cell> cells = new ArrayList<>();
                for (int i = 0; i < csvCells.length; i++) {
                    //TODO si son mas 29 celdas, ver que pasaria con los otros codigos ASCII...
                    String col = String.valueOf( (char)(65 + i) );
                    Cell cell = new Cell<>(csvCells[i],rows,col);
                    cells.add(cell);
                }
                book.getActiveSheet().getRows().put(rows,cells);
                rows++;
            }
            br.close();
        } catch (IOException  e) {
            System.out.println("IO Exception...");
        }
        return book;
    }

    public String getValuesSeparator() {
        return valuesSeparator;
    }

    public void setValuesSeparator(String valuesSeparator) {
        this.valuesSeparator = valuesSeparator;
    }

}
