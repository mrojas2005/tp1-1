package ar.fiuba.tdd.tp1.model.functions;

/**
 * Created by Nico on 21/9/2015. Defines methods for commands.
 *
 */
public interface FunctionsCommand {

    Object undo();

    Object redo();

}
