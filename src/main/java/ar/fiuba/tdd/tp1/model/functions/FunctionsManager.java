package ar.fiuba.tdd.tp1.model.functions;

/**
 * Created by Nico on 21/9/2015.
 *  Singleton made in order to have only 1 pair of undo/redo stacks.
 */
public class FunctionsManager {

    private static final FunctionsManager instance = new FunctionsManager();
    private UndoRedoCommand undoRedoCommand;


    public static FunctionsManager getInstance() {
        return instance;
    }

    private FunctionsManager() {
        this.undoRedoCommand = new UndoRedoCommand();
    }

    public Object undo() {
        return this.undoRedoCommand.undo();
    }

    public Object redo() {
        return this.undoRedoCommand.redo();
    }

    public void addOperation(FunctionsCommand functionsCommand) {
        this.undoRedoCommand.addUndoFunction(functionsCommand);
    }

}
