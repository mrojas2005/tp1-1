package ar.fiuba.tdd.tp1.model.functions;

import java.util.Stack;

/**
 * Created by Nico on 4/10/2015. Manages the functions undo/redo Stack.
 * */

public class UndoRedoCommand {

    private Stack<FunctionsCommand> commandsUndoHistory;
    private Stack<FunctionsCommand> commandsRedoHistory;

    public UndoRedoCommand() {
        commandsUndoHistory = new Stack<>();
        commandsRedoHistory = new Stack<>();
    }

    public final Object undo() {
        FunctionsCommand functionToUndo = this.commandsUndoHistory.pop();
        this.commandsRedoHistory.push(functionToUndo);
        return functionToUndo.undo();
    }

    public final Object redo() {
        FunctionsCommand command = this.commandsRedoHistory.pop();
        Object object = command.redo();
        this.commandsUndoHistory.push(command);
        return object;
    }

    public final void addUndoFunction(final FunctionsCommand function) {
        this.commandsUndoHistory.push(function);
    }

}
