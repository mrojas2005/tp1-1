package ar.fiuba.tdd.tp1.model.functions.functionsimpl;

import ar.fiuba.tdd.tp1.model.functions.FunctionsCommand;

/**
 * Created by Nico on 4/10/2015.
 * Implements the undo/redo values updates when needed.
 */
public class ChangedCommand implements FunctionsCommand {

    private Object oldValue;
    private Object actualValue;
    private Object newValue;

    @Override
    public Object undo() {
        this.setRedoValues(actualValue, oldValue);
        actualValue = oldValue;

        return actualValue;
    }

    @Override
    public Object redo() {
        actualValue = newValue;
        return actualValue;
    }

    public void setUndoValues(Object oldCell, Object actualCell) {
        this.oldValue = oldCell;
        this.actualValue = actualCell;
    }

    public void setRedoValues(Object newCell, Object actualCell) {
        this.newValue = newCell;
        this.actualValue = actualCell;
    }

}
