package ar.fiuba.tdd.tp1.acceptance;

import ar.fiuba.tdd.tp1.acceptance.driver.BadFormulaException;
import ar.fiuba.tdd.tp1.acceptance.driver.Grupo1409Test;
import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTestDriver;
import ar.fiuba.tdd.tp1.model.Money;
import org.junit.Before;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * Created by marcelo on 12/10/15.
 * Tests sobre formatters
 */
public class FormatterTest {
    private SpreadSheetTestDriver testDriver;

    @Before
    public void setUp() {
        testDriver = new Grupo1409Test();
    }

    @Test
    public void testDecimalsOnNumbers() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", -1.12345);
        testDriver.setCellValue("tecnicas", "default", "A2", -123456789.12345);
        testDriver.setDecimals("tecnicas", "default", "A1", 3);
        testDriver.setDecimals("tecnicas", "default", "A2", 3);
        testDriver.setCellValue("tecnicas", "default", "B2", 2);
        assertEquals("-1.123", testDriver.getFormattedOutput("tecnicas", "default", "A1"));
        assertEquals("-123,456,789.123", testDriver.getFormattedOutput("tecnicas", "default", "A2"));
    }

    @Test
    public void testDecimalsOnMoney() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "Z1", new Money(23.4));
        testDriver.setCellValue("tecnicas", "default", "X2", new Money(123456789.12345));
        testDriver.setDecimals("tecnicas", "default", "Z1", 3);
        testDriver.setDecimals("tecnicas", "default", "X2", 3);
        assertEquals("ARS 23.4", testDriver.getFormattedOutput("tecnicas", "default", "Z1"));
        assertEquals("ARS 123,456,789.123", testDriver.getFormattedOutput("tecnicas", "default", "X2"));
    }

    @Test
    public void testCurrencyOnMoney() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "Z1", new Money(23.4));
        testDriver.setCellValue("tecnicas", "default", "X2", new Money("USD", 10200));
        assertEquals("ARS 23", testDriver.getFormattedOutput("tecnicas", "default", "Z1"));
        assertEquals("USD 10,200", testDriver.getFormattedOutput("tecnicas", "default", "X2"));
        testDriver.setCurrency("tecnicas", "default", "X2", "GBP");
        assertEquals("GBP 10,200", testDriver.getFormattedOutput("tecnicas", "default", "X2"));

    }

    @Test
    public void testFormatonDates() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String fecha = "23-01-1999";
        Date fechaAsDate;

        try {
            fechaAsDate = dateFormat.parse(fecha);
        } catch (Exception e) {
            throw new BadFormulaException();
        }

        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "Z1", fechaAsDate);
        testDriver.setDateFormat("tecnicas", "default", "Z1", "MM-dd-yyyy");
        assertEquals("01-23-1999",testDriver.getFormattedOutput("tecnicas","default","Z1"));

    }
}
