package ar.fiuba.tdd.tp1.acceptance;

import ar.fiuba.tdd.tp1.acceptance.driver.BadFormulaException;
import ar.fiuba.tdd.tp1.acceptance.driver.Grupo1409Test;
import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTestDriver;
import ar.fiuba.tdd.tp1.model.Money;
import org.junit.Before;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/*
* Tests for group 1409
* Put here all tests related with simple data types
* */
public class SimpleValuesTest {

    private static final double DELTA = 0.0001;
    private SpreadSheetTestDriver testDriver;

    @Before
    public void setUp() {
        testDriver = new Grupo1409Test();
    }

    @Test
    public void setAndRetrieveStringValue() {
        testDriver.createNewWorkBookNamed("tecnicas");

        testDriver.setCellValue("tecnicas", "default", "A1", "Hello World!");

        assertEquals("Hello World!", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }

    @Test // Modificado para iteración 2
    public void setAndRetrieveNumberValue() {
        testDriver.createNewWorkBookNamed("tecnicas");

        testDriver.setCellValue("tecnicas", "default", "A1", 1000.50);

        assertEquals(1000.50, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    @Test
    public void setAndRetrieveMultipleNumberValues1() {
        testDriver.createNewWorkBookNamed("tecnicas");

        testDriver.setCellValue("tecnicas", "default", "A1", 1000.50);
        testDriver.setCellValue("tecnicas", "default", "A2", 10);
        testDriver.setCellValue("tecnicas", "default", "A3", 1);
        testDriver.setCellValue("tecnicas", "default", "A4", -10.50);
        testDriver.setCellValue("tecnicas", "default", "A5", 0.0);

        assertEquals(1000.50, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
        assertEquals(10, testDriver.getCellValueAsDouble("tecnicas", "default", "A2"), DELTA);
        assertEquals(1, testDriver.getCellValueAsDouble("tecnicas", "default", "A3"), DELTA);
        assertEquals(-10.50, testDriver.getCellValueAsDouble("tecnicas", "default", "A4"), DELTA);
        assertEquals(0.0, testDriver.getCellValueAsDouble("tecnicas", "default", "A5"), DELTA);
    }

    @Test
    public void setAndRetrieveMultipleNumberValues2() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String fecha = "23-01-1999";
        Date fechaAsDate;
        final Money money = new Money(23.15);

        try {
            fechaAsDate = dateFormat.parse(fecha);
        } catch (Exception e) {
            throw new BadFormulaException();
        }

        testDriver.createNewWorkBookNamed("tecnicas");

        // Test of Number
        testDriver.setCellValue("tecnicas", "default", "A1", 1000.50);
        testDriver.setCellValue("tecnicas", "default", "A2", 10);

        // Test of Date and Money data types

        testDriver.setCellValue("tecnicas", "default", "A3", fechaAsDate);
        testDriver.setCellValue("tecnicas", "default", "A4", money);

        assertEquals(1000.50, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
        assertEquals(10, testDriver.getCellValueAsDouble("tecnicas", "default", "A2"), DELTA);

        // Test of Date
        assertEquals(fechaAsDate.toString(), testDriver.getCellValueAsDate("tecnicas", "default", "A3").toString());

        // Test of money
        assertEquals(23.15, testDriver.getCellValueAsMoney("tecnicas", "default", "A4").getAmount(), DELTA);
    }

    @Test
    public void updateCellValue() {
        testDriver.createNewWorkBookNamed("tecnicas");

        testDriver.setCellValue("tecnicas", "default", "A1", 1000.50);
        assertEquals(1000.50, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);

        testDriver.setCellValue("tecnicas", "default", "A1", "Hello");
        assertEquals("Hello", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }


}