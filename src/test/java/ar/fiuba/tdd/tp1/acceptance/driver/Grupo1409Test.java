package ar.fiuba.tdd.tp1.acceptance.driver;

import ar.fiuba.tdd.tp1.model.*;

import java.text.DecimalFormatSymbols;
import java.util.*;

/**
 * Created by marcelo on 10/5/15.
 * Grupo 1409 -> Tests
 */
public class Grupo1409Test implements SpreadSheetTestDriver {


    private FiubaSpreadSheet spreadSheet = new SpreadSheet();

    public Grupo1409Test() {
        Locale locale = new Locale("es", "AR");

        DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
        symbols.setDecimalSeparator('.');
        symbols.setGroupingSeparator(',');

    }

    @Override
    public List<String> workBooksNames() {
        return this.spreadSheet.getWorkbookNames();
    }

    @Override
    public void createNewWorkBookNamed(String name) {
        this.spreadSheet.createBook(name);
    }

    @Override
    public void createNewWorkSheetNamed(String workbookName, String name) {
        this.spreadSheet.createSheet(workbookName, name);
    }

    @Override
    public List<String> workSheetNamesFor(String workBookName) {
        return this.spreadSheet.workSheetNamesFor(workBookName);
    }

    private String convertToFullRef(String expression, String workSheetName) {
        List<String> tokens = new ArrayList<>(Arrays.asList(expression.substring(2).split(" ")));
        String result = "=";
        for (String token : tokens) {
            if (!ExpressionTerms.isValidTerm(token, ExpressionTerms.CELLREF)
                    && ExpressionTerms.isValidTerm(token, ExpressionTerms.ROWCOLREF)) {
                result = result + " " + workSheetName + ":" + token;
            } else {
                result = result + " " + token;
            }
        }
        return result;
    }

    @Override
    public void setCellValue(String workBookName, String workSheetName, String cellId, Object value) {
        try {
            String parsedValue;
            if (value instanceof String) {
                parsedValue = (String) value;
                if (ExpressionTerms.isValidTerm(parsedValue, ExpressionTerms.CELLFORMULA)) {
                    this.spreadSheet.setCellValue(workBookName, workSheetName, cellId, parsedValue.replaceAll("!", ""));
                } else {
                    this.spreadSheet.setCellValue(workBookName, workSheetName, cellId, value);
                }
            } else {
                this.spreadSheet.setCellValue(workBookName, workSheetName, cellId, value);
            }
        } catch (Exception e) {
            throw new BadFormulaException();
        }
    }

    public void setDecimals(String workBookName, String workSheetName, String cellId, int decimals) {
        Cell cell = this.spreadSheet.getBook(workBookName).getSheet(workSheetName).getCell(cellId);
        if (cell != null) {
            cell.setDecimals(decimals);
        } else {
            throw new IllegalArgumentException("Cell does not exist.");
        }
    }

    public String getFormattedOutput(String workBookName, String workSheetName, String cellId) {
        Cell cell = this.spreadSheet.getBook(workBookName).getSheet(workSheetName).getCell(cellId);
        if (cell != null) {
            return cell.getFormattedData();
        } else {
            throw new IllegalArgumentException("Error al obtener celda");
        }
    }

    public void setCurrency(String workBookName, String workSheetName, String cellId, String currencyCode) {
        try {
            Cell cell = this.spreadSheet.getBook(workBookName).getSheet(workSheetName).getCell(cellId);
            if (cell != null) {
                cell.setCurrencyCode(currencyCode);
            }
        } catch (Exception exception) {
            throw new IllegalArgumentException("Error al setear moneda");
        }

    }

    public void setDateFormat(String workBookName, String workSheetName, String cellId, String dateFormat) {
        try {
            Cell cell = this.spreadSheet.getBook(workBookName).getSheet(workSheetName).getCell(cellId);
            if (cell != null) {
                cell.setOutputFormatter(dateFormat);
            }
        } catch (Exception exception) {
            throw new BadFormulaException();
        }
    }

    @Override
    public String getCellValueAsString(String workBookName, String workSheetName, String cellId) {
        try {
            return (String) this.spreadSheet.getCellValueByName(workBookName, workSheetName
                    + ExpressionTerms.CELLREFSEP + cellId);
        } catch (Exception exception) {
            throw new BadFormulaException();
        }
    }

    public Money getCellValueAsMoney(String workBookName, String workSheetName, String cellId) {
        try {
            return (Money) this.spreadSheet.getCellValueByName(workBookName, workSheetName
                    + ExpressionTerms.CELLREFSEP + cellId);
        } catch (Exception exception) {
            throw new IllegalArgumentException("Error obteniendo celda como Money");
        }
    }

    @Override
    public double getCellValueAsDouble(String workBookName, String workSheetName, String cellId) {
        try {
            Object result = this.spreadSheet.getCellValueByName(workBookName, workSheetName
                    + ExpressionTerms.CELLREFSEP + cellId);
            if (Number.class.isAssignableFrom(result.getClass())) {
                return Double.parseDouble(result.toString());
            } else {
                throw new IllegalArgumentException("Invalid number conversion");
            }
        } catch (Exception exception) {
            throw new BadFormulaException();
        }

    }

    public Date getCellValueAsDate(String workBookName, String workSheetName, String cellId) {
        try {
            return (Date) this.spreadSheet.getCellValueByName(workBookName, workSheetName
                    + ExpressionTerms.CELLREFSEP + cellId);
        } catch (Exception e) {
            throw new BadFormulaException();
        }
    }

    @Override
    public void undo() {
        this.spreadSheet.undo();
    }

    @Override
    public void redo() {
        this.spreadSheet.redo();
    }
}