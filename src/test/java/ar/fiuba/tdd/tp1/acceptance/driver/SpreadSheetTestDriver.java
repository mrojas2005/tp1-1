package ar.fiuba.tdd.tp1.acceptance.driver;

import ar.fiuba.tdd.tp1.model.Money;

import java.util.Date;
import java.util.List;

public interface SpreadSheetTestDriver {

    List<String> workBooksNames();

    void createNewWorkBookNamed(String name);

    void createNewWorkSheetNamed(String workbookName, String name);

    List<String> workSheetNamesFor(String workBookName);

    void setCellValue(String workBookName, String workSheetName, String cellId, Object value);

    String getCellValueAsString(String workBookName, String workSheetName, String cellId);

    double getCellValueAsDouble(String workBookName, String workSheetName, String cellId);

    Date getCellValueAsDate(String workBookName, String workSheetName, String cellId);

    Money getCellValueAsMoney(String workBookName, String workSheetName, String cellId);

    String getFormattedOutput(String workBookName, String workSheetName, String cellId);

    void setDecimals(String workBookName, String workSheetName, String cellId, int decimals);

    void setCurrency(String workBookName, String workSheetName, String cellId, String currencyCode);

    void setDateFormat(String workBookName, String workSheetName, String cellId, String dateFormat);


    void undo();

    void redo();
}
